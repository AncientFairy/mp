package com.example.mp;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.example.mp.domain.User;
import com.example.mp.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author 许大仙
 * @version 1.0
 * @since 2021-06-18 20:31
 */
@SpringBootTest
public class TestDelete {
    @Autowired
    private UserMapper userMapper;

    @Test
    public void test() {
        LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(User::getUsername, "zhangsan");

        int result = userMapper.delete(wrapper);
        System.out.println("result = " + result);
    }
}
