package com.example.mp;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.mp.domain.User;
import com.example.mp.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * @author 许大仙
 * @version 1.0
 * @since 2021-06-18 22:36
 */
@SpringBootTest
public class TestSelectPage {

    @Autowired
    private UserMapper userMapper;

    @Test
    public void test(){
        Page<User> page = new Page<>(2,1);

        Page<User> selectPage = userMapper.selectPage(page, new QueryWrapper<>());

        long total = selectPage.getTotal();
        long pages = selectPage.getPages();
        System.out.println("数据总条数 = " + total);
        System.out.println("总页数 = " + pages);

        List<User> userList = selectPage.getRecords();
        System.out.println("userList = " + userList);
    }
}
