package com.example.mp;

import com.example.mp.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 许大仙
 * @version 1.0
 * @since 2021-06-18 20:10
 */
@SpringBootTest
public class TestDeleteByMap {
    @Autowired
    private UserMapper userMapper;

    @Test
    public void test(){
        Map<String,Object> map = new HashMap<>();
        map.put("id", "5");
        map.put("age", "24");

        //将map中的元素设置为删除的条件，多个条件之间是and的关系
        int result = userMapper.deleteByMap(map);
        System.out.println("result = " + result);
    }
}
