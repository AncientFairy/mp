package com.example.mp;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.example.mp.domain.User;
import com.example.mp.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.mybatis.spring.MyBatisSystemException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author 许大仙
 * @version 1.0
 * @since 2021-06-18 19:22
 */
@SpringBootTest
public class TestUpdate {
    @Autowired
    private UserMapper userMapper;

    @Test
    public void testUpdateById() {

        User user = new User();
        user.setId(1L);
        user.setUsername("许大仙");

        //根据id更新，更新不为null的字段，返回更新的条数
        int result = userMapper.updateById(user);
        System.out.println("result = " + result);

    }

    @Test
    public void testUpdate() {
        //封装更新字段的对象
        User user = new User();
        user.setAge(18);

        //封装更新条件的对象
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_name", "zhangsan");

        int result = userMapper.update(user, queryWrapper);
        System.out.println("result = " + result);
    }

    @Test
    public void testUpdate2() {
        UpdateWrapper<User> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("age", 18)
                .set("password", "123456").eq("user_name", "zhangsan");

        int result = userMapper.update(null, updateWrapper);
        System.out.println("result = " + result);
    }


    @Test
    public void test() {
        //封装更新字段的对象
        User user = new User();
        user.setAge(18);

        try {
            int result = userMapper.update(user, null);
            System.out.println("result = " + result);
        } catch (MyBatisSystemException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void testOptimisticLocker() {
        //先查询再修改，会触发乐观锁配置，即实体对象中要有version字段值
        User user = userMapper.selectById(1);

        user.setUsername("许大仙");

        int result = userMapper.updateById(user);
        System.out.println("result = " + result);
    }

}
