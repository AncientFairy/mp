package com.example.mp;

import com.example.mp.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author 许大仙
 * @version 1.0
 * @since 2021-06-18 20:04
 */
@SpringBootTest
public class TestDeleteById {
    @Autowired
    private UserMapper userMapper;

    @Test
    public void test(){
        //执行删除操作
        int result = userMapper.deleteById(6);
        System.out.println("result = " + result);
    }

}
