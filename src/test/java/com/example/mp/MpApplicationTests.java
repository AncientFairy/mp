package com.example.mp;

import com.example.mp.domain.User;
import com.example.mp.enums.SexEnum;
import com.example.mp.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;


@SpringBootTest
class MpApplicationTests {

    @Autowired
    private UserMapper userMapper;

    @Test
    public void testInsert(){
        User user = new User();
        user.setUsername("wangba");
        user.setPassword("123456");
        user.setName("王八");
        user.setAge(18);
        user.setEmail("123456@qq.com");
        user.setSex(SexEnum.WOMAN);

        int count = userMapper.insert(user);
        System.out.println("count = " + count); //数据库受影响的行数
        System.out.println("user.getId() = " + user.getId()); //自增后的id会回填到对象中
    }

    @Test
    public void test(){
        List<User> userList = userMapper.findAll();
        System.out.println("userList = " + userList);
    }


}
