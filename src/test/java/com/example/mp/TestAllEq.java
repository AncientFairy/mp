package com.example.mp;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.mp.domain.User;
import com.example.mp.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 许大仙
 * @version 1.0
 * @since 2021-06-19 11:04
 */
@SpringBootTest
public class TestAllEq {

    @Autowired
    private UserMapper userMapper;

    @Test
    public void testAllEq() {
        QueryWrapper<User> wrapper = new QueryWrapper<>();

        //设置条件
        Map<String,Object> params = new HashMap<>();
        params.put("name","zhangsan");
        params.put("age", null);

        //SELECT id,user_name,password,name,age,email FROM tb_user WHERE (name = ? AND age IS NULL)
        wrapper.allEq(params);

        List<User> userList = userMapper.selectList(wrapper);
        System.out.println("userList = " + userList);

    }

    @Test
    public void testAllEqNull2IsNullFalse() {
        QueryWrapper<User> wrapper = new QueryWrapper<>();

        //设置条件
        Map<String,Object> params = new HashMap<>();
        params.put("name","zhangsan");
        params.put("age", null);

        //SELECT id,user_name,password,name,age,email FROM tb_user WHERE (name = ?)
        wrapper.allEq(params,false);

        List<User> userList = userMapper.selectList(wrapper);
        System.out.println("userList = " + userList);

    }

}
