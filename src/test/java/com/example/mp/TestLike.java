package com.example.mp;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.mp.domain.User;
import com.example.mp.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * @author 许大仙
 * @version 1.0
 * @since 2021-06-19 11:27
 */
@SpringBootTest
public class TestLike {
    @Autowired
    private UserMapper userMapper;

    @Test
    public void test() {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        //SELECT id,user_name,password,name,age,email FROM tb_user WHERE (name LIKE %张%)
        wrapper.like("name", "张");

        List<User> userList = userMapper.selectList(wrapper);
        System.out.println("userList = " + userList);
    }

}
