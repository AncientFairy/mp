package com.example.mp;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.example.mp.domain.User;
import com.example.mp.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author 许大仙
 * @version 1.0
 * @since 2021-06-18 22:21
 */
@SpringBootTest
public class TestSelectCount {

    @Autowired
    private UserMapper userMapper;

    @Test
    public void test() {
        //根据条件查询数据条数
        Integer count = userMapper.selectCount(new LambdaQueryWrapper<User>().ge(User::getId, 2));
        System.out.println("count = " + count);
    }

}
