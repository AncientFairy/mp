package com.example.mp;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.example.mp.domain.User;
import com.example.mp.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * @author 许大仙
 * @version 1.0
 * @since 2021-06-18 22:14
 */
@SpringBootTest
public class TestSelectList {

    @Autowired
    private UserMapper userMapper;

    @Test
    public void test() {

        LambdaQueryWrapper<User> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.ge(User::getId, 3);

        //根据条件查询数据
        List<User> userList = userMapper.selectList(lambdaQueryWrapper);
        System.out.println("userList = " + userList);
    }
}
